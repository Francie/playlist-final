#include "MusicMenu.h"
#include <ctime>

int main () 
{

	list Music;
	int MusicplayList=1;
	int MusicQueue=1;
	//START
	Music.pushNode("","","","",0);
	
	
	//Cutterpillow  13
	Music.pushNode("Superproxy - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Back2me - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Waiting for the Bus - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Fine Time - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Kama Supra - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Overdrive - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Slo Mo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Torpedo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Huwag mo nang Itanong - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Paru-Parong Ningning - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Walang Nagbago - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Cutterpillow - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Poorman's Grave - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	//  Save Rock & Roll
	Music.pushNode("The Phoenix - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("My Songs Know What You Did in the Dark (Light Em Up) - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Alone Together - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Where Did the Party Go - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Just One Yesterday - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("The Mighty Fall - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Miss Missing You - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Death Valley - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Young Volcanoes - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Rat a Tat - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Save Rock and Roll - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	// Clarity
	Music.pushNode("Hourglass - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Shave it - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Spectrum - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Lost at Sea - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Clarity - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Codec - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Stache - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Fall in to Sky - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Follow you Down - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Epos - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Stay The Night - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Push Play - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Alive - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Breakin a Sweat - Zedd", "Zedd", "POP", "Clarity",5);
	//NOthing Personal
	Music.pushNode("Weightless - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Break Your Little Heart - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Damned If I Do Ya, Damned If I Don't - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Lost In Stereo - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Stella - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Sick Little Games - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Hello, Brooklyn - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Walls - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Too Much - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Keep The Change, You Filthy Animal - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("A Party Song (The Walk Of Shame) - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Therapy - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	//All We Know is Falling
	Music.pushNode("Pressure - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("All We Know - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Emergency - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Brighter - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Here We Go Again - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Let This Go - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Woah - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Conspiracy - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Franklin - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("My Heart - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	//If You Were a Movie, This Would Be Your Soundtrack
	Music.pushNode("Scene One - James Dean & Audrey Hepburn - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	Music.pushNode("Scene Two - Roger Rabbit - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	Music.pushNode("Scene Three - Stomach Tied In Knots - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	Music.pushNode("Scene Four - Don't You Ever Forget About Me - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	Music.pushNode("Scene Five - With Ears To See and Eyes To Hear - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	//The Finer Things (Acoustic)
	Music.pushNode("Elevated - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Deadly Conversation - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Hard To Please - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Prepare to be Noticed - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Over The Line - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Sample Existence - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Remedy - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Nothing's Wrong - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Mind Bottled - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Critical - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Easy Enough - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	//Science & Faith
	Music.pushNode("Nothing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("If you ever come back - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Science & Faith - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Long Gone and Moved on - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Deadman Walking - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("This Love - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Walk Away - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Exit Wounds - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("You won't feel a Thing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	//Move Along
	Music.pushNode("Dirty Little Secret - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Stab My Back - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Move Along - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("It Ends Tonight - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Change Your Mind - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Night Drive - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("11:11 PM - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Dance Inside - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Top of the World - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Straitjacket Feeling  - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("I'm waiting - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Can't take it - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	//Sweetener
	Music.pushNode("God Is A Woman - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Sweetener - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Breathin - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("No Tears Left To Cry - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Everytime - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Get Well Soon- Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Goodnight n Go - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("R.E.M. - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("The Light is Coming - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	//Red
	Music.pushNode("State of Grace - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("Red - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("I Knew You Were Trouble - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("All Too Well - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("22- Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("I Almost Do - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("We Are Never Ever Getting Back Together - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("Starlight - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("Holy Ground - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("Begin Again- Taylor Swift", "Taylor Swift", "POP", "Red",5);
	//Hopeless Fountain Kingdom
	Music.pushNode("Eyes Closed - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Alone - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Now or Never - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Sorry - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Bad At Love - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Devil In Me - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	//Honey Works
	Music.pushNode("Assertion of the Heart - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Bae Love - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Bloom in Love Color	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Brazen Honey - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Can I confess to you? - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("The day I knew Love	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Declaration of the Weak	 - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Dream Fanfare - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Fansa - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("I like you now - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Inokori-sensei - HoneyWorks feat. flower", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Light Proof Theory - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Love meets and love continues - HoneyWorks feat. Kotoha", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Maidens	- HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Mama - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Monday's Melancholy - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Nostalgic Rainfall - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Picture Book of my first love - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Secret of Sunday - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Sick name love wazurai - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("A small lion - HoneyWorks feat. Minami & LIPxLIP", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Tokyo summer session - HoneyWorks feat. Gumi & flower", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Twins - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	//Mind Of Mine
	Music.pushNode("PILLOWTALK - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?iT’s YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("BeFoUr - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("sHe - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("dRuNk - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?rEaR vIeW - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?wRoNg - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?fOoL fOr YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("BoRdErZ - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?tRuTh - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("lUcOzAdE - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("TiO - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("BLUE - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("BRIGHT - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("LIKE I WOULD - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("SHE DON’T LOVE ME - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
		//PERIPHERAL VISION 10
	Music.pushNode("Dizzy On The Comedown - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Cutting My Fingers Off - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Take My Head - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Diazepam - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("New Scream - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Threshold - TurnOver","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Humming - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Like Slowly Disappearing - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("I Would Hate You If I Could - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Intrapersonal - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	//FOREST HILLS DRIVE 2014 13
	Music.pushNode("Intro - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("January 28th - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Wet Dreamz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("03' Adolescence' - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("A tale of two Citiez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("FireSquad - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("St Tropez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Hello - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("G.O.M.D - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("No Role Modelz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Apparently - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Love Yourz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Note To Self - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	// WE ARE NOT YOUR KIND 14
	Music.pushNode("Nero Forte - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Spiders - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);	
	Music.pushNode("Insert Coin - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Unsainted - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("A Lier's Funeral - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Solway Firth - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Birth Of the Cruel - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Orphan - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Critical Darling - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("My Pain - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Red Flag - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Not Long for this world - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Death because of death - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("What's Next? - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	//Enema of the State 12
	Music.pushNode("Dumpweed - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Dont Leave Me - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Aliens Exist - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Going Away To College - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("What's My Age Again' - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Dysentry Gary - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Adam's Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("All The Small Things - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("The Party Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Mutt - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Wendy Clear - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Anthem - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	//American FootBall 9
	Music.pushNode("Never Meant - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("The Summer Ends - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("Honestly - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("For Sure - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("You Know I Should Be Leaving Soon - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("But The Regrets Are Killing Me - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("I'll See You When We're Both Not So Emotional - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("Stay Home - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("The One With The Wurlitzer - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	//LNOTGY 10
	Music.pushNode("Citezens Of Earth - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("I Hope This Comes Back To Haunt You - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("December - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Smooth Seas Don’t Make Good Sailors - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Cant Kick Up The Roots - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Gold Steps - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Kali Ma - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Rock Bottom - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("The Beach is For Lovers Not Losers - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Serpents - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	//Doo-Wops & Hooligans 11
	Music.pushNode("Grenade - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Just the Way you Are - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Our First Time - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Runaway Baby - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("The Lazy Song - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Marry You - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Talking to the Moon - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Liquor Store Blues - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Count on Me - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("The Other Side - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Somewhere in Brooklyn - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	//FOUR
	Music.pushNode("Steal My Girl - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Ready to Run - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Where Do Broken Hearts Go - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("18 - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Girl Almighty - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Fool’s Gold - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Night Changes - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("No Control - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Fireproof - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Spaces - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Stockholm Syndrome - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Clouds - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Change Your Ticket - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Illusion - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Once in a Lifetime - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Act My Age - One Direction", "One Direction", "POP", "FOUR",5);
	//Multiply
	Music.pushNode("One - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("I'm a mess - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Sing - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Don't - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Nina - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Photograph - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Bloodstream - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Terenife Sea - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Thinking out loud - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	//Divide
	Music.pushNode("Eraser - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Castle on the hill - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Dive - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Shape of you - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Perfect - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Galway Girl - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Happier - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("New man - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Hearts don't break around here - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("What do i know - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("How do you feel - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Supermarket Flowers - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Barcelona - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Bibia be ye ye - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Nancy Mulligan - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Save yourself - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	//1989
	Music.pushNode("Welcome To New York - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Blank Space - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Style - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Out Of The Woods - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("All you had to do was stay - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Shake it Off - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("I wish you would - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Bad Blood - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Wildest Dreams - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("How You Get The Girl - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("This Love - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("I know Places - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Clean - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	//After Laughter
	Music.pushNode("Hard Times - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Rose-Colored Boy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Told You So - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Forgiveness - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Fake Happy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("26 - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Pool - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Grudges - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Caught In the Middle - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Idle Worship - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("No Friend - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Tell Me How - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	//Need You Now
	Music.pushNode("Need You Now - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Our Kind Of Love - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("American Honey - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Hello World - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Perfect Day - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Love This Pain - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("When You Got A Good Thing - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Stars Tonight - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("If I Know Then - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Something Bout A Woman - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	//HeartBreak
	Music.pushNode("Home - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Heart Break - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Somebody's Else's Heart - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("This City - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Hurt - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Army - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Good Time To Be Alive - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Think About You - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Big Love In A Small Town - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	//Symphony Soldier
	Music.pushNode("Angel With A Shotgun - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Endlessly  - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Animal - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Intoxicated - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Lala - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Her Love Is My Religion - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Another Me - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Lovesick Fool - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	//Freudian
	Music.pushNode("Get You - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Best Part - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Hold Me DOwn - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Nue Roses - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Loose - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("We Find Love - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Blessed - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Take Me Away - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Transform - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Freudian - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	//Case Study 01
	Music.pushNode("LOVE AGAIN - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("SUPER POSITION - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("TOO DEEP TO TURN BACK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("FRONTAL LOBE MUZIK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("CYANIDE - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("ARE YOU OKAY? - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("OPEN UP - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("ENTROPY - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("RESTORE THE FEELING - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("COMPLEXITIES - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("loveAgain - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	// Natty Dread
	Music.pushNode("Lively Up Yourself - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Them Belly Full - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Rebel Music - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("So Jah Seh - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Natty Dread - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Bend Down Low - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Talkin Blues - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Ravolution - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	//Legend
	Music.pushNode("Is This Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Could You Be Loved - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Three Little Birds - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Buffalo Soldier - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Get Up Stand Up- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Stir It Up - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("One Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("I Shot The Sheriff - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Waiting In Vain - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Redemption Song - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Satisfy My Soul - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Exodus - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Jamming - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	//Rastaman Vibrations
	Music.pushNode("Positive Vibrations - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Roots, Rocks, Reggae - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Johnny Was - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Cry To Me - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Want More - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Crazy Bald Head - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Who The Cap Fit- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Night Shift - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("War - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Rat Race - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	//4 your Eyez only
	Music.pushNode("4 Your Eyez Only - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Change - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Deja Vu - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Foldin Clothes - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("For Whom The Bells Toll - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Immortal - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Neighbors - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("She's Mine Pt. 1 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("She's Mine Pt. 2 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Ville Mentality - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	//Russ The Mixtape
	Music.pushNode("For the Stunt - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Aint NobodyTakin My Baby - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("2 A.M. - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Down For You - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Off The Strength - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("We Just Havent Met Yet - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Always Knew- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Whenever- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Waste My Time- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Lost- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Yung God- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Lately- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Comin' Thru- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Connected- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Keep the Faith- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("All My Angels- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Brush Me (feat. Colliee Buddz)- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	//Kupal Destroyerr 1
	Music.pushNode("Lamasin Ang Osus- TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Animal - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Coming - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Makapal ang Blbl mo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Sir tngna mo pkyu ALbum VEr - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Isubo ang ulo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Ptngna 2 - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("PtngnaTrapik ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Sira Ulo ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Ptngna 1 - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Anak ka ng Pakyu Album Ver - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Kupal Ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Galit ako sa Gwapo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Mamatay ka na - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Mamatay ka na sana - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Gard Tngna mo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("BAstos Ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);	
	Music.pushNode("Mamatay ka na sana Album VEr- TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Sir Tanginamo Pakyuuu - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	//December Avenue (2017)
	Music.pushNode("City Lights - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Sleep Tonight - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Fallin’ - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Ears and Rhymes - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("I’ll Be Watching You - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Back To Love - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Eroplanong Papel - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Dive - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Forever - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Breathe Again - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Time To Go - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	//Too Weird To Live, Too Rare To Die!
	Music.pushNode("Casual Affair - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Collar Full  - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Girl That You Love - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Miss Jackson - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Nicotine - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("The End Of All Things - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("This Is Gospel - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Vegas Lights - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	//Master Of Puppets
	Music.pushNode("Battery - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Damage, Inc. - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Disposable Heroes - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Leper Messiah - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Master Of Puppets - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Orion - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("The Thing That Should Be - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Welcome Home (Sanitarium) - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	//Get Your Heart On!
	Music.pushNode("You Suck at Love - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Can't Keep My Hands off You - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Jet Lag - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Astronaut - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Loser of the Year - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Anywhere Else But Here - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Freaking Me Out - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Summer Paradise - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Gone Too Soon - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Last One Standing - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("This Song Saved My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	//Still Not Getting Any
	Music.pushNode("Shut Up! - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Welcome to My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Perfect World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Thank You - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Me Against the World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Jump - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Everytime - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Promise - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("One - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Untitled - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Perfect - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	//Vessels
	Music.pushNode("Ode To sleep - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Migraine - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("House Of gold - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("car Radio - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Semi Automatic - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("SCreen - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("The Run and Go - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Fake You Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Guns for Hands - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Trees - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Truce - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	//Blurry Face
	Music.pushNode("Heavy Dirty Soul - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Stressed Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Ride - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Fairly Local - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Tear in my heart - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Lane Boy - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("The Judge - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Doubt - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Polarize - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("We Dont Believe What's on TV - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Message Man - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Home Town - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Not Today - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Goner - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);

	Music.pushNode("Dizzy On The Comedown - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Cutting My Fingers Off - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Take My Head - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Diazepam - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("New Scream - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Threshold - TurnOver","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Humming - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Like Slowly Disappearing - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("I Would Hate You If I Could - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	Music.pushNode("Intrapersonal - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	//FOREST HILLS DRIVE 2014 13
	Music.pushNode("Intro - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("January 28th - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Wet Dreamz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("03' Adolescence' - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("A tale of two Citiez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("FireSquad - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("St Tropez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Hello - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("G.O.M.D - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("No Role Modelz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Apparently - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Love Yourz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	Music.pushNode("Note To Self - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	// WE ARE NOT YOUR KIND 14
	Music.pushNode("Nero Forte - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Spiders - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);	
	Music.pushNode("Insert Coin - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Unsainted - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("A Lier's Funeral - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Solway Firth - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Birth Of the Cruel - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Orphan - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Critical Darling - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("My Pain - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Red Flag - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Not Long for this world - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("Death because of death - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	Music.pushNode("What's Next? - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	//Enema of the State 12
	Music.pushNode("Dumpweed - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Dont Leave Me - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Aliens Exist - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Going Away To College - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("What's My Age Again' - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Dysentry Gary - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Adam's Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("All The Small Things - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("The Party Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Mutt - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Wendy Clear - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	Music.pushNode("Anthem - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	//American FootBall 9
	Music.pushNode("Never Meant - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("The Summer Ends - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("Honestly - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("For Sure - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("You Know I Should Be Leaving Soon - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("But The Regrets Are Killing Me - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("I'll See You When We're Both Not So Emotional - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("Stay Home - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	Music.pushNode("The One With The Wurlitzer - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	//LNOTGY 10
	Music.pushNode("Citezens Of Earth - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("I Hope This Comes Back To Haunt You - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("December - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Smooth Seas Don’t Make Good Sailors - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Cant Kick Up The Roots - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Gold Steps - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Kali Ma - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Rock Bottom - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("The Beach is For Lovers Not Losers - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	Music.pushNode("Serpents - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	//Doo-Wops & Hooligans 11
	Music.pushNode("Grenade - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Just the Way you Are - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Our First Time - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Runaway Baby - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("The Lazy Song - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Marry You - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Talking to the Moon - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Liquor Store Blues - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Count on Me - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("The Other Side - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	Music.pushNode("Somewhere in Brooklyn - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	//Cutterpillow  13
	Music.pushNode("Superproxy - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Back2me - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Waiting for the Bus - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Fine Time - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Kama Supra - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Overdrive - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Slo Mo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Torpedo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Huwag mo nang Itanong - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Paru-Parong Ningning - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Walang Nagbago - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Cutterpillow - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	Music.pushNode("Poorman's Grave - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	//  Save Rock & Roll
	Music.pushNode("The Phoenix - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("My Songs Know What You Did in the Dark (Light Em Up) - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Alone Together - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Where Did the Party Go - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Just One Yesterday - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("The Mighty Fall - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Miss Missing You - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Death Valley - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Young Volcanoes - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Rat a Tat - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	Music.pushNode("Save Rock and Roll - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	// Clarity
	Music.pushNode("Hourglass - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Shave it - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Spectrum - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Lost at Sea - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Clarity - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Codec - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Stache - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Fall in to Sky - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Follow you Down - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Epos - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Stay The Night - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Push Play - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Alive - Zedd", "Zedd", "POP", "Clarity",5);
	Music.pushNode("Breakin a Sweat - Zedd", "Zedd", "POP", "Clarity",5);
	//NOthing Personal
	Music.pushNode("Weightless - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Break Your Little Heart - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Damned If I Do Ya, Damned If I Don't - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Lost In Stereo - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Stella - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Sick Little Games - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Hello, Brooklyn - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Walls - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Too Much - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Keep The Change, You Filthy Animal - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("A Party Song (The Walk Of Shame) - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	Music.pushNode("Therapy - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	//All We Know is Falling
	Music.pushNode("Pressure - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("All We Know - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Emergency - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Brighter - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Here We Go Again - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Let This Go - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Woah - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Conspiracy - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("Franklin - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	Music.pushNode("My Heart - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	//If You Were a Movie, This Would Be Your Soundtrack
	Music.pushNode("Scene One - James Dean & Audrey Hepburn - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	Music.pushNode("Scene Two - Roger Rabbit - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	Music.pushNode("Scene Three - Stomach Tied In Knots - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	Music.pushNode("Scene Four - Don't You Ever Forget About Me - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	Music.pushNode("Scene Five - With Ears To See and Eyes To Hear - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	//The Finer Things (Acoustic)
	Music.pushNode("Elevated - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Deadly Conversation - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Hard To Please - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Prepare to be Noticed - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Over The Line - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Sample Existence - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Remedy - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Nothing's Wrong - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Mind Bottled - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Critical - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	Music.pushNode("Easy Enough - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	//Science & Faith
	Music.pushNode("Nothing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("If you ever come back - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Science & Faith - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Long Gone and Moved on - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Deadman Walking - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("This Love - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Walk Away - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("Exit Wounds - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	Music.pushNode("You won't feel a Thing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	//Move Along
	Music.pushNode("Dirty Little Secret - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Stab My Back - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Move Along - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("It Ends Tonight - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Change Your Mind - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Night Drive - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("11:11 PM - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Dance Inside - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Top of the World - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Straitjacket Feeling  - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("I'm waiting - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	Music.pushNode("Can't take it - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	//Sweetener
	Music.pushNode("God Is A Woman - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Sweetener - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Breathin - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("No Tears Left To Cry - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Everytime - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Get Well Soon- Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("Goodnight n Go - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("R.E.M. - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	Music.pushNode("The Light is Coming - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	//Red
	Music.pushNode("State of Grace - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("Red - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("I Knew You Were Trouble - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("All Too Well - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("22- Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("I Almost Do - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("We Are Never Ever Getting Back Together - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("Starlight - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("Holy Ground - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	Music.pushNode("Begin Again- Taylor Swift", "Taylor Swift", "POP", "Red",5);
	//Hopeless Fountain Kingdom
	Music.pushNode("Eyes Closed - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Alone - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Now or Never - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Sorry - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Bad At Love - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	Music.pushNode("Devil In Me - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	//Honey Works
	Music.pushNode("Assertion of the Heart - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Bae Love - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Bloom in Love Color	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Brazen Honey - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Can I confess to you? - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("The day I knew Love	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Declaration of the Weak	 - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Dream Fanfare - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Fansa - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("I like you now - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Inokori-sensei - HoneyWorks feat. flower", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Light Proof Theory - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Love meets and love continues - HoneyWorks feat. Kotoha", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Maidens	- HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Mama - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Monday's Melancholy - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Nostalgic Rainfall - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Picture Book of my first love - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Secret of Sunday - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Sick name love wazurai - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("A small lion - HoneyWorks feat. Minami & LIPxLIP", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Tokyo summer session - HoneyWorks feat. Gumi & flower", "HoneyWorks", "ROCK", "HoneyWorks",12);
	Music.pushNode("Twins - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	//Mind Of Mine
	Music.pushNode("PILLOWTALK - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?iT’s YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("BeFoUr - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("sHe - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("dRuNk - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?rEaR vIeW - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?wRoNg - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?fOoL fOr YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("BoRdErZ - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("?tRuTh - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("lUcOzAdE - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("TiO - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("BLUE - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("BRIGHT - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("LIKE I WOULD - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	Music.pushNode("SHE DON’T LOVE ME - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	//FOUR
	Music.pushNode("Steal My Girl - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Ready to Run - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Where Do Broken Hearts Go - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("18 - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Girl Almighty - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Fool’s Gold - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Night Changes - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("No Control - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Fireproof - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Spaces - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Stockholm Syndrome - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Clouds - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Change Your Ticket - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Illusion - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Once in a Lifetime - One Direction", "One Direction", "POP", "FOUR",5);
	Music.pushNode("Act My Age - One Direction", "One Direction", "POP", "FOUR",5);
	//Multiply
	Music.pushNode("One - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("I'm a mess - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Sing - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Don't - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Nina - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Photograph - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Bloodstream - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Terenife Sea - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	Music.pushNode("Thinking out loud - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	//Divide
	Music.pushNode("Eraser - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Castle on the hill - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Dive - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Shape of you - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Perfect - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Galway Girl - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Happier - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("New man - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Hearts don't break around here - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("What do i know - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("How do you feel - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Supermarket Flowers - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Barcelona - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Bibia be ye ye - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Nancy Mulligan - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	Music.pushNode("Save yourself - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	//1989
	Music.pushNode("Welcome To New York - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Blank Space - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Style - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Out Of The Woods - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("All you had to do was stay - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Shake it Off - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("I wish you would - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Bad Blood - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Wildest Dreams - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("How You Get The Girl - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("This Love - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("I know Places - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	Music.pushNode("Clean - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	//After Laughter
	Music.pushNode("Hard Times - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Rose-Colored Boy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Told You So - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Forgiveness - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Fake Happy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("26 - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Pool - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Grudges - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Caught In the Middle - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Idle Worship - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("No Friend - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	Music.pushNode("Tell Me How - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	//Need You Now
	Music.pushNode("Need You Now - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Our Kind Of Love - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("American Honey - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Hello World - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Perfect Day - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Love This Pain - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("When You Got A Good Thing - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Stars Tonight - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("If I Know Then - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	Music.pushNode("Something Bout A Woman - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	//HeartBreak
	Music.pushNode("Home - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Heart Break - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Somebody's Else's Heart - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("This City - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Hurt - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Army - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Good Time To Be Alive - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Think About You - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	Music.pushNode("Big Love In A Small Town - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	//Symphony Soldier
	Music.pushNode("Angel With A Shotgun - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Endlessly  - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Animal - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Intoxicated - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Lala - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Her Love Is My Religion - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Another Me - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	Music.pushNode("Lovesick Fool - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	//Freudian
	Music.pushNode("Get You - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Best Part - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Hold Me DOwn - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Nue Roses - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Loose - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("We Find Love - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Blessed - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Take Me Away - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Transform - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	Music.pushNode("Freudian - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	//Case Study 01
	Music.pushNode("LOVE AGAIN - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("SUPER POSITION - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("TOO DEEP TO TURN BACK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("FRONTAL LOBE MUZIK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("CYANIDE - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("ARE YOU OKAY? - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("OPEN UP - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("ENTROPY - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("RESTORE THE FEELING - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("COMPLEXITIES - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	Music.pushNode("loveAgain - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	// Natty Dread
	Music.pushNode("Lively Up Yourself - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Them Belly Full - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Rebel Music - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("So Jah Seh - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Natty Dread - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Bend Down Low - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Talkin Blues - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	Music.pushNode("Ravolution - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	//Legend
	Music.pushNode("Is This Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Could You Be Loved - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Three Little Birds - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Buffalo Soldier - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Get Up Stand Up- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Stir It Up - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("One Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("I Shot The Sheriff - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Waiting In Vain - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Redemption Song - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Satisfy My Soul - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Exodus - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	Music.pushNode("Jamming - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	//Rastaman Vibrations
	Music.pushNode("Positive Vibrations - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Roots, Rocks, Reggae - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Johnny Was - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Cry To Me - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Want More - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Crazy Bald Head - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Who The Cap Fit- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Night Shift - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("War - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	Music.pushNode("Rat Race - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	//4 your Eyez only
	Music.pushNode("4 Your Eyez Only - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Change - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Deja Vu - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Foldin Clothes - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("For Whom The Bells Toll - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Immortal - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Neighbors - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("She's Mine Pt. 1 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("She's Mine Pt. 2 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	Music.pushNode("Ville Mentality - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	//Russ The Mixtape
	Music.pushNode("For the Stunt - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Aint NobodyTakin My Baby - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("2 A.M. - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Down For You - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Off The Strength - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("We Just Havent Met Yet - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Always Knew- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Whenever- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Waste My Time- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Lost- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Yung God- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Lately- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Comin' Thru- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Connected- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Keep the Faith- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("All My Angels- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	Music.pushNode("Brush Me (feat. Colliee Buddz)- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	//Kupal Destroyerr 1
	Music.pushNode("Lamasin Ang Osus- TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Animal - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Coming - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Makapal ang Blbl mo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Sir tngna mo pkyu ALbum VEr - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Isubo ang ulo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Ptngna 2 - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("PtngnaTrapik ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Sira Ulo ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Ptngna 1 - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Anak ka ng Pakyu Album Ver - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Kupal Ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Galit ako sa Gwapo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Mamatay ka na - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Mamatay ka na sana - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Gard Tngna mo - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("BAstos Ka - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);	
	Music.pushNode("Mamatay ka na sana Album VEr- TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	Music.pushNode("Sir Tanginamo Pakyuuu - TUBERO", "TUBERO", "METAL", "Kupal Destroyerr 1",6);
	//December Avenue (2017)
	Music.pushNode("City Lights - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Sleep Tonight - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Fallin’ - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Ears and Rhymes - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("I’ll Be Watching You - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Back To Love - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Eroplanong Papel - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Dive - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Forever - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Breathe Again - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	Music.pushNode("Time To Go - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	//Too Weird To Live, Too Rare To Die!
	Music.pushNode("Casual Affair - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Collar Full  - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Girl That You Love - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Miss Jackson - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Nicotine - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("The End Of All Things - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("This Is Gospel - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	Music.pushNode("Vegas Lights - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	//Master Of Puppets
	Music.pushNode("Battery - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Damage, Inc. - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Disposable Heroes - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Leper Messiah - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Master Of Puppets - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Orion - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("The Thing That Should Be - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	Music.pushNode("Welcome Home (Sanitarium) - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	//Get Your Heart On!
	Music.pushNode("You Suck at Love - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Can't Keep My Hands off You - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Jet Lag - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Astronaut - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Loser of the Year - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Anywhere Else But Here - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Freaking Me Out - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Summer Paradise - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Gone Too Soon - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("Last One Standing - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	Music.pushNode("This Song Saved My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	//Still Not Getting Any
	Music.pushNode("Shut Up! - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Welcome to My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Perfect World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Thank You - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Me Against the World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Jump - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Everytime - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Promise - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("One - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Untitled - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	Music.pushNode("Perfect - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	//Vessels
	Music.pushNode("Ode To sleep - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Migraine - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("House Of gold - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("car Radio - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Semi Automatic - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("SCreen - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("The Run and Go - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Fake You Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Guns for Hands - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Trees - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Truce - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	Music.pushNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	//Blurry Face
	Music.pushNode("Heavy Dirty Soul - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Stressed Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Ride - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Fairly Local - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Tear in my heart - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Lane Boy - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("The Judge - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Doubt - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Polarize - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("We Dont Believe What's on TV - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Message Man - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Home Town - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Not Today - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	Music.pushNode("Goner - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);

	

//START OF THE PROGRAM
	int xRan;
	srand( time(0));
	//xRan=rand()%10+1;
	
	bool systemLoopCondition = true;
	while(systemLoopCondition == true)
	{
		cin.clear();
		system("CLS");
		int number;
		cout << "SONG PLAYLIST COUNTER: "<< MusicplayList-1 <<endl
			 << "SONG QUEUE COUNTER: "<< MusicQueue-1 <<endl
			 
		
			<< " 1 : Song Library Playlist                    "<< endl
			<< " 2 : Song Playlist                            "<< endl
			<< " 3 : View Queue                               "<< endl
			<< " 4 : Sorting                                  "<< endl
			<< " 5 : Quit                                     "<< endl															  
			<< "  "<< endl;

			cout<< "Select your number of choice:  ";
			cin >> number;
//////////ERROR CATCHER
		if(cin.fail())
		{
			cin.clear();
			cin.ignore();
		}
//////////CHOICE 1
		else if (number == 1)
		{
	
			Music.display();
			cout << "No More Songs are Available\n";
			system("PAUSE");
		}
//////////CHOICE 2
		else if (number ==2)
		{	
		
//			SetConsoleTextAttribute(hConsole, 11);  
			Music.viewPlaylist();
//			SetConsoleTextAttribute(hConsole, 13);  
			int selectplaylist;
			int songchoice;
			cout <<"1: Add to the playlist" << endl
				 <<"2: Remove from the playlist" <<endl
				 <<"3: Play the Playlist" <<endl
				 <<"4: Back" << endl;
			cin >> selectplaylist;
			if(cin.fail()||selectplaylist>4||selectplaylist<1)
			{
	//			SetConsoleTextAttribute(hConsole, 4);  
				cout << "Input Error\n";
				system("PAUSE");
			}
			else if(selectplaylist==1)
			{
			//	SetConsoleTextAttribute(hConsole, 11);  
				Music.display();
			//	SetConsoleTextAttribute(hConsole, 13);  
				cout << "Enter Number of the Song you want to add to the Playlist > ";
				cin >> songchoice;
				MusicplayList = Music.addToPlaylist(songchoice,MusicplayList)+MusicplayList;
				system("PAUSE");
			}
			else if(selectplaylist==2)
			{
				MusicplayList =MusicplayList-Music.remFrmPlaylist(MusicplayList);
			}
			else if(selectplaylist==3)
			{
				Music.playStack(MusicplayList);
			}
			else if(selectplaylist==4)
			{
			}
			else
			{
			}
		}
//////////CHOICE 3
		else if (number==3)
		{
		//			SetConsoleTextAttribute(hConsole, 11);  
			Music.printQueue(MusicQueue);
//			SetConsoleTextAttribute(hConsole, 13);  
			int selectplaylist;
			int songchoice;
			cout <<"1: Add to the Queue" << endl
				 <<"2: Remove from the Queue" <<endl
				 <<"3: Play the Queue" <<endl
				 <<"4: Back" << endl;
			cin >> selectplaylist;
			if(cin.fail()||selectplaylist>4||selectplaylist<1)
			{
	//			SetConsoleTextAttribute(hConsole, 4);  
				cout << "Input Error.\n";
				system("PAUSE");
			}
			else if(selectplaylist==1)
			{
			//	SetConsoleTextAttribute(hConsole, 11);  
				Music.display();
			//	SetConsoleTextAttribute(hConsole, 13);  
				cout << "Select Number of the Song you want to Add to Queue > ";
				cin >> songchoice;
				MusicQueue = Music.enQueue(songchoice,MusicQueue)+MusicQueue;
				//ADD TO QUEUE / ENQUEUE
				system("PAUSE");
			}
			else if(selectplaylist==2)
			{
				//REMOVE FROM QUEUE / DEQUEUE
				MusicQueue=MusicQueue-Music.deQueue();
				if(MusicQueue<=0)
				{
					MusicQueue=1;
				}
			}
			else if(selectplaylist==3)
			{
				Music.playQueue(MusicQueue);
			}
			else if(selectplaylist==4)
			{
			}
			else
			{
			}
		}
//////////CHOICE 4 / SORT
		else if (number==4)
		{
			system("CLS");
			Music.sortByAlbum();
			system("PAUSE");
		} 
//////////CHOICE 5 / RANDOM PLAYLIST
		else if (number==5)
		{
			return 0;
		}
	}
} 